import 'package:flutter/material.dart';

import './dummy_data.dart';
import './screens/tabs_screen.dart';
import './screens/jadwal_detail_screen.dart';
import './screens/category_jadwal_screen.dart';
import './screens/categories_screen.dart';
import './models/jadwal.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  List<Jadwal> _availableJadwal = DUMMY_SCHEDULE;
  List<Jadwal> _favoriteJadwal = [];


  void _toggleFavorite(String jadwalId) {
    final existingIndex =
    _favoriteJadwal.indexWhere((jadwal) => jadwal.id == jadwalId);
    if (existingIndex >= 0) {
      setState(() {
        _favoriteJadwal.removeAt(existingIndex);
      });
    } else {
      setState(() {
        _favoriteJadwal.add(
          DUMMY_SCHEDULE.firstWhere((jadwal) => jadwal.id == jadwalId),
        );
      });
    }
  }

  bool _isMealFavorite(String id) {
    return _favoriteJadwal.any((jadwal) => jadwal.id == id);
  }

  MaterialColor buildMaterialColor(Color color) {
    List strengths = <double>[.05];
    Map swatch = <int, Color>{};
    final int r = color.red, g = color.green, b = color.blue;

    for (int i = 1; i < 10; i++) {
      strengths.add(0.1 * i);
    }
    strengths.forEach((strength) {
      final double ds = 0.5 - strength;
      swatch[(strength * 1000).round()] = Color.fromRGBO(
        r + ((ds < 0 ? r : (255 - r)) * ds).round(),
        g + ((ds < 0 ? g : (255 - g)) * ds).round(),
        b + ((ds < 0 ? b : (255 - b)) * ds).round(),
        1,
      );
    });
    return MaterialColor(color.value, swatch);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Ngingetin',
      theme: ThemeData(
        primarySwatch: buildMaterialColor(Color(0xFF1C2A35)),
        accentColor: Color(0xFF1C2A35),
        canvasColor: Color(0xFF171E24),
        fontFamily: 'Raleway',
        textTheme: ThemeData.light().textTheme.copyWith(
            bodyText1: TextStyle(
              color: Colors.white,
            ),
            bodyText2: TextStyle(
              color: Colors.white,
            ),
            headline6: TextStyle(
              fontSize: 20,
              fontFamily: 'RobotoCondensed',
              fontWeight: FontWeight.bold,
            )),
      ),
      // home: CategoriesScreen(),
      initialRoute: '/', // default is '/'
      routes: {
        '/': (ctx) => TabsScreen(_favoriteJadwal),
        CategoryJadwalScreen.routeName: (ctx) =>
            CategoryJadwalScreen(_availableJadwal),
        MealDetailScreen.routeName: (ctx) =>
            MealDetailScreen(_toggleFavorite, _isMealFavorite),
      },
      onGenerateRoute: (settings) {
        print(settings.arguments);
      },
      onUnknownRoute: (settings) {
        return MaterialPageRoute(
          builder: (ctx) => CategoriesScreen(),
        );
      },
    );
  }
}
