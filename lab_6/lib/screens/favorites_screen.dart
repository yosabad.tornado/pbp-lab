import 'package:flutter/material.dart';

import '../models/jadwal.dart';
import '../widgets/jadwal_item.dart';

class FavoritesScreen extends StatelessWidget {
  final List<Jadwal> favoriteJadwal;

  FavoritesScreen(this.favoriteJadwal);

  @override
  Widget build(BuildContext context) {
    if (favoriteJadwal.isEmpty) {
      return Center(
        child: Text('Belum ada yang dipilih.'),
      );
    } else {
      return ListView.builder(
        itemBuilder: (ctx, index) {
          return MealItem(
            id: favoriteJadwal[index].id,
            title: favoriteJadwal[index].title,
            imageUrl: favoriteJadwal[index].imageUrl,
            duration: favoriteJadwal[index].duration,
          );
        },
        itemCount: favoriteJadwal.length,
      );
    }
  }
}
