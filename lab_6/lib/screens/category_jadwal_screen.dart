import 'package:flutter/material.dart';

import '../widgets/jadwal_item.dart';
import '../models/jadwal.dart';

class CategoryJadwalScreen extends StatefulWidget {
  static const routeName = '/category-meals';

  final List<Jadwal> availableJadwal;

  CategoryJadwalScreen(this.availableJadwal);

  @override
  _CategoryJadwalScreenState createState() => _CategoryJadwalScreenState();
}

class _CategoryJadwalScreenState extends State<CategoryJadwalScreen> {
  String categoryTitle;
  List<Jadwal> displayedMeals;
  var _loadedInitData = false;

  @override
  void initState() {
    // ...
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (!_loadedInitData) {
      final routeArgs =
          ModalRoute.of(context).settings.arguments as Map<String, String>;
      categoryTitle = routeArgs['title'];
      final categoryId = routeArgs['id'];
      displayedMeals = widget.availableJadwal.where((meal) {
        return meal.categories.contains(categoryId);
      }).toList();
      _loadedInitData = true;
    }
    super.didChangeDependencies();
  }

  void _removeMeal(String jadwalId) {
    setState(() {
      displayedMeals.removeWhere((jadwal) => jadwal.id == jadwalId);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(categoryTitle),
      ),
      body: ListView.builder(
        itemBuilder: (ctx, index) {
          return MealItem(
            id: displayedMeals[index].id,
            title: displayedMeals[index].title,
            imageUrl: displayedMeals[index].imageUrl,
            duration: displayedMeals[index].duration,
          );
        },
        itemCount: displayedMeals.length,
      ),
    );
  }
}
