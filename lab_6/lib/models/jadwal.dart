import 'package:flutter/foundation.dart';

enum Complexity {
  Simple,
  Challenging,
  Hard,
}

enum Affordability {
  Affordable,
  Pricey,
  Luxurious,
}

class Jadwal {
  final String id;
  final List<String> categories;
  final String title;
  final String imageUrl;
  final List<String> notes;
  final int duration;
  final Complexity complexity;
  final Affordability affordability;

  const Jadwal({
    this.id,
    this.categories,
    this.title,
    this.imageUrl,
    this.notes,
    this.duration,
    this.complexity,
    this.affordability,
  });
}
