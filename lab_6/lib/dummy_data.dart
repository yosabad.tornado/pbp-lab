import 'package:flutter/material.dart';

import './models/category.dart';
import './models/jadwal.dart';

const DUMMY_CATEGORIES = const [
  Category(
    id: 'c1',
    title: 'SDA',
    color: Color(0xFF1C2A35),
  ),
  Category(
    id: 'c2',
    title: 'PBP',
    color: Color(0xFF1C2A35),
  ),
  Category(
    id: 'c3',
    title: 'MANBIS',
    color: Color(0xFF1C2A35),
  ),
  Category(
    id: 'c4',
    title: 'MATDIS',
    color: Color(0xFF1C2A35),
  ),
];

const DUMMY_SCHEDULE = const [
  Jadwal(
    id: 'm1',
    categories: [
      'c1',
    ],
    title: 'Hari-hari meng-SDA',
    imageUrl: 'https://i.ytimg.com/vi/AE5I0xACpZs/maxresdefault.jpg',
    duration: 20,
    notes: [
      'Pelajaran ini membuatku pusing 7 keliling',
    ],
  ),
  Jadwal(
    id: 'm2',
    categories: [
      'c2',
    ],
    title: 'PBP kusuka',
    imageUrl:
        'https://unida.ac.id/teknologi/assets/images/post/663a4ca1cc26a87557b2118d94e10174.jpg',
    duration: 20,
    notes: [
      'PBP seru tapi terburu-buru',
    ],
  ),
  Jadwal(
    id: 'm3',
    categories: [
      'c3',
    ],
    title: 'Manbis kesukaanku',
    imageUrl:
        'https://image.shutterstock.com/image-vector/business-management-chart-keywords-icons-600w-441319423.jpg',
    duration: 20,
    notes: [
      'Mari berbisnis!',
    ],
  ),
  Jadwal(
    id: 'm4',
    categories: [
      'c4',
    ],
    title: 'Matdis kesukaanku',
    imageUrl:
        'https://olcovers2.blob.core.windows.net/coverswp/2018/01/Discrete-Mathematics-OpenLibra.gif',
    duration: 20,
    notes: [
      'AWW',
    ],
  ),
];
