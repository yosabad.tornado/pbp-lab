import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: "Ngingetin!",
    home: BelajarForm(),
  ));
}

class BelajarForm extends StatefulWidget {
  @override
  _BelajarFormState createState() => _BelajarFormState();
}

class _BelajarFormState extends State<BelajarForm> {
  final _formKey = GlobalKey<FormState>();

  double nilaiSlider = 1;
  bool nilaiCheckBox = false;
  bool nilaiSwitch = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF171E24),
      appBar: AppBar(
        backgroundColor: Color(0xFF1C2A35),
        title: Text("Schedule"),
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      hoverColor: Colors.white,
                      hintText: "e.g. PBP",
                      hintStyle: TextStyle(color: Color(0xFF555555)),
                      labelText: "Enter title",
                      labelStyle: TextStyle(color: Color(0xFF777777)),
                      icon: Icon(
                        Icons.title,
                        color: Color(0xFF999999),
                      ),
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    style: TextStyle(color: Colors.white),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Please fill out this field!';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    obscureText: true,
                    decoration: new InputDecoration(
                      hoverColor: Colors.white,
                      hintText: "e.g. Monday",
                      hintStyle: TextStyle(color: Color(0xFF555555)),
                      labelText: "Day",
                      labelStyle: TextStyle(color: Color(0xFF777777)),
                      icon: Icon(
                        Icons.date_range_rounded,
                        color: Color(0xFF999999),
                      ),
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Please fill out this field!';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    obscureText: true,
                    decoration: new InputDecoration(
                      hoverColor: Colors.white,
                      labelText: "Time",
                      labelStyle: TextStyle(color: Color(0xFF777777)),
                      icon: Icon(
                        Icons.access_time_sharp,
                        color: Color(0xFF999999),
                      ),
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Please fill out this field!';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    obscureText: true,
                    decoration: new InputDecoration(
                      hoverColor: Colors.white,
                      hintText: "e.g. Nothing here",
                      hintStyle: TextStyle(color: Color(0xFF555555)),
                      labelText: "Enter notes",
                      labelStyle: TextStyle(color: Color(0xFF777777)),
                      icon: Icon(
                        Icons.date_range_rounded,
                        color: Color(0xFF999999),
                      ),
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Please fill out this field!';
                      }
                      return null;
                    },
                  ),
                ),
                // CheckboxListTile(
                //   title: Text('Belajar Dasar Flutter'),
                //   subtitle: Text('Dart, widget, http'),
                //   value: nilaiCheckBox,
                //   activeColor: Colors.deepPurpleAccent,
                //   onChanged: (value) {
                //     setState(() {
                //       nilaiCheckBox = value!;
                //     });
                //   },
                // ),
                // SwitchListTile(
                //   title: Text('Backend Programming'),
                //   subtitle: Text('Dart, Nodejs, PHP, Java, dll'),
                //   value: nilaiSwitch,
                //   activeTrackColor: Colors.pink[100],
                //   activeColor: Colors.red,
                //   onChanged: (value) {
                //     setState(() {
                //       nilaiSwitch = value;
                //     });
                //   },
                // ),
                // Slider(
                //   value: nilaiSlider,
                //   min: 0,
                //   max: 100,
                //   onChanged: (value) {
                //     setState(() {
                //       nilaiSlider = value;
                //     });
                //   },
                // ),
                SizedBox(
                  height: 20.0,
                ),
                RaisedButton(
                  child: Text(
                    "Submit",
                    style: TextStyle(color: Colors.white),
                  ),
                  color: Colors.red,
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {}
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
