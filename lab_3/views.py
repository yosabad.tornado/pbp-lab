from django.contrib.auth.decorators import login_required
from django.http import response
from django.shortcuts import render
from .forms import FriendForm
from lab_1.models import Friend

# Create your views here.

@login_required(login_url="/admin/login")
def index(request):
    friends = Friend.objects.all()
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)
    
@login_required(login_url="/admin/login")
def add_friend(request):
    context ={}
    form = FriendForm(request.POST or None)
    if (form.is_valid and request.method == "POST"):
        form.save()
        return response.HttpResponseRedirect('/lab_3')
    context['form']= form
    return render(request, "lab3_form.html", context)
    
