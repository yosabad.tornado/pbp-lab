from django.urls import path
from .views import index, friend_list

urlpatterns = [
    path('', index, name='index'),
    path('friend/', friend_list, name='friend_list'),
    # TODO Add friends path using friend_list Views
]
