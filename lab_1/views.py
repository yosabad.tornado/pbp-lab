from django.shortcuts import render
from datetime import datetime, date
from .models import Friend
from django.http import response

mhs_name = 'Yosabad Torando Sirait'  # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2001, 10, 22)  # TODO Implement this, format (Year, Month, Date)
npm = 2006597046  # TODO Implement this


def index(request):
    response = {'name': mhs_name,
                'age': calculate_age(birth_date.year),
                'npm': npm,
                'kelas' : "pbp-d"}
    return render(request, 'index_lab1.html', response)


def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0


def friend_list(request):
    friends = Friend.objects.all()  # TODO Implement this
    response = {'friends': friends}
    return render(request, 'friend_list_lab1.html', response)
