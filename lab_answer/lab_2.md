<!-- 
Yosabad Torando Sirait
2006597046 
-->

1. Apakah perbedaan antara JSON dan XML?
    JSON dan XML merupakan sebuah format untuk menyimpan dan mengirimkan data. Keduanya adalah teknologi terkait web
    untuk pertukaran data. JSON dan XML memiliki perbedaan. JSON (JavaScript Object Notation) merupakan format data
    yang digunakan untuk menyimpan informasi secara terorganisasi sehingga terlihat lebih logis dan mudah dibaca oleh
    manusia. Sedangkan XML merupakan bahasa markup yang digunakan untuk mengangkut data dari satu aplikasi ke aplikasi
    lain melalui internet dalam bentuk dokumen/informasi metadata. Jadi JSON lebih berorientasi pada data sedangkan XML
    berorientasi pada dokumen.

2. Apakah perbedaan antara HTML dan XML?
    HTML dan XML merupakan bahasa markup yang sama-sama menggunakan tag dalam mendefinisikan setiap elemen pada dokumen.
    Meskipun sama, keduanya ternyata memiliki perbedaan. HTML lebih menitikberatkan bagaimana tampilan dari data sehingga
    menarik untuk dilihat tanpa ada informasi dari data yang dibuat, sedangkan XML menitikberatkan pada struktur/bentuk 
    dan konteksnya dengan melihat informasi di dalam data sehingga data terlihat lebih terstruktur.

Referensi:
    Risya, R. 2020. Apa Perbedaan JSON Dan XML?. Diakses melalui https://www.monitorteknologi.com/perbedaan-json-dan-xml/
    WikiBuku. Pemrograman XML/XML vs HTML. Diakses melalui https://id.wikibooks.org/wiki/Pemrograman_XML/XML_vs_HTML
